#Add to basket

##Quick start

Once project is cloned, run the following commands:

```
yarn
```

```
yarn start
```

To run tests:

```
yarn test
```

## Tech stack

The tech used is as follows:

- [React](https://reactjs.org/)
- [Create-React-App](https://github.com/facebook/create-react-app)
- [Node](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/lang/en/)
- [Webpack](https://webpack.js.org/)
- [Redux](https://redux.js.org/introduction)

Styling with:

- [StyledComponents](https://www.styled-components.com/)
- [MaterialUi](https://material-ui.com/)

Testing with:

- [Jest](https://facebook.github.io/jest/)

## Redux set-up

#### Middleware

A 'middlewareList' array has been created in store/index.js to allow other middlewares (e.g. thunk or redux-observable) to be added at a later stage. Currently the only middleware in the project is Redux Logger.

### API integration

The application does not currently integrate with an API. If data was to be fetched for the products, this would be included as part of an app initialise function with data fed into Redux store (productState) on success. This could be handled by middleware such as [Redux-Observable](https://github.com/redux-observable/redux-observable) or [Redux-Thunk](https://github.com/reduxjs/redux-thunk).

### Application structure

Reducers, actions and default state have been separated as they will be acting on separate slices of the application state. Currently only the basketState requires reducers/actions but combineReducers() has been included in root-reducer.js to allow further reducers to be added as the app scales.

### Handling actions

The application does not use the traditional switch statement to handle actions and instead uses the below function based on the 'type' field. I chose this as it allows code to be split/refactored more easily as the app grows larger/more complex.

```
const productsReducer = (state = defaultProductsState, action) => {
  return handlers.hasOwnProperty(action.type)
    ? handlers[action.type](state, action.payload)
    : state;
};
```

## Adding and removing from the basket

The key logic for the application currently sits within the basket reducer.

Functions for adding/removing products from the basket, updating the total basket count and updating the total basket price have been extracted into helper functions inside basketHelpers.js. This allows the functions to be re-used elsewhere if required and also means they can be tested separately.

## The basket drawer

I've chosen to render the shopping basket inside a drawer that is collapsed by default as I believe this provides a more familiar user journey. It also allows the product page to work on smaller devices. When the user clicks on a product to "Add to basket", the basket icon at the top will show a count to ensure the user knows that the product has been added successfully.

On opening the basket, the user will then see a list of all products added, their quantities, prices and a total basket price.

From there the user can remove products one at a time by clicking on the cross to the right of the product. Given more time I would add plus and minus icon buttons to allow the user to increment or decrement the products in the basket.

The basketContent connected container is rendered as a child of the drawer to prevent unnecessary re-rendering of the drawer component. It would also allow this drawer to be re-used in other areas of the site if further logic was written to handle the rendering of different components.

## Testing

Testing has been set up using Jest.

Jest tests have been written to cover the key application logic in the basketHelper functions as well as the basketReducer. With more time, ideally the application would also include some component snapshot testing using a testing utility such as [Enzyme](https://github.com/airbnb/enzyme).

## App.js

Currently App.js only returns the product page. In a live application with more pages this is where app routing would ideally be handled.

## Styling

Styling has been handled with a combination of materialUI and styled components.

### Responsive styling

Responsive styling is handled by MaterialUI breakpoints on MUI components such as the table in the basket drawer. Elsewhere, breakpoints are included using styled components and have been set up to automatically generate using a helper function in styles/breakpoints.js.
