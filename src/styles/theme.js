import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#33a0ff",
      main: "#008aff",
      dark: "#006dcc",
      contrastText: "#ffffff"
    }
  },
  typography: {
    fontFamily: "'Montserrat', sans-serif",
    headline: {
      fontSize: "1rem"
    }
  },
  overrides: {
    MuiButton: {
      root: {
        letterSpacing: 2,
        padding: 20,
        fontSize: 11
      }
    }
  }
});
