import * as React from "react";
import { Provider } from "react-redux";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { theme } from "./styles/theme.js";
import { AppWrapper } from "./components/appWrapper";

const App = ({ store }) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <AppWrapper />
      </Provider>
    </MuiThemeProvider>
  );
};

export default App;
