export const mockProduct = {
  productCode: "001",
  productName: "Travel Card Holder",
  productPrice: 9.25
};

export const mockAddedProductOne = {
  productCode: "001",
  productName: "Travel Card Holder",
  productPrice: 9.25,
  basketPrice: 9.25,
  quantity: 1
};

export const mockAddedProductTwo = {
  productCode: "002",
  productName: "Personalised Cufflinks",
  productPrice: 45,
  basketPrice: 45,
  quantity: 1
};

export const mockAddedProducts = [mockAddedProductOne, mockAddedProductTwo];
