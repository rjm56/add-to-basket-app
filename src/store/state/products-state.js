export const defaultProductsState = {
  products: [
    {
      productCode: "001",
      productName: "Travel Card Holder",
      productPrice: 9.25,
      productImg: "/assets/images/travelCard.jpg"
    },
    {
      productCode: "002",
      productName: "Personalised Cufflinks",
      productPrice: 45,
      productImg: "/assets/images/cufflinks.jpg"
    },
    {
      productCode: "003",
      productName: "Kids T-shirt",
      productPrice: 19.95,
      productImg: "/assets/images/tshirt.jpg"
    }
  ]
};
