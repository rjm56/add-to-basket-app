export const defaultBasketState = {
  basketItems: [],
  basketCount: 0,
  basketTotal: 0,
  basketOpen: false
};
