import { createStore, applyMiddleware, compose } from "redux";
import { logger } from "redux-logger";
import { rootReducer } from "./root-reducer";

const middlewareList = [logger];

const middlewares = compose(applyMiddleware(...middlewareList));

const store = createStore(rootReducer, middlewares);

export { store };
