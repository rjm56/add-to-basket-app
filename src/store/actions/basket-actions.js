export const basketActions = {
  ADD_TO_BASKET: `ADD_TO_BASKET`,
  REMOVE_ONE_FROM_BASKET: `REMOVE_ONE_FROM_BASKET`,
  REMOVE_ALL_FROM_BASKET: `REMOVE_ALL_FROM_BASKET`,
  TOGGLE_BASKET_DRAWER: `TOGGLE_BASKET_DRAWER`
};

export const addToBasket = product => ({
  type: basketActions.ADD_TO_BASKET,
  payload: product
});

export const removeOneFromBasket = product => ({
  type: basketActions.REMOVE_ONE_FROM_BASKET,
  payload: product
});

export const toggleBasketDrawer = open => ({
  type: basketActions.TOGGLE_BASKET_DRAWER,
  payload: open
});
