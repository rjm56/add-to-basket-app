import { defaultProductsState } from "../state";

const handlers = {};

const basketReducer = (state = defaultProductsState, action) => {
  return handlers.hasOwnProperty(action.type)
    ? handlers[action.type](state, action.payload)
    : state;
};

export default basketReducer;
