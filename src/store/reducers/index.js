import productReducer from "./product-reducer";
import basketReducer from "./basket-reducer";

export const reducers = {
  productState: productReducer,
  basketState: basketReducer
};
