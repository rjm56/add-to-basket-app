import { defaultBasketState } from "../state";
import { basketActions } from "../actions";
import {
  updateBasketInfo,
  addItem,
  removeOneItem
} from "../../helpers/basket-helpers.js";

const handlers = {
  [basketActions.ADD_TO_BASKET]: (state, payload) => {
    const newBasketItems = addItem(state.basketItems, payload);
    const basketCount = updateBasketInfo(newBasketItems, "quantity");
    const basketTotal = updateBasketInfo(newBasketItems, "basketPrice");

    return {
      ...state,
      basketItems: newBasketItems,
      basketCount,
      basketTotal
    };
  },

  [basketActions.REMOVE_ONE_FROM_BASKET]: (state, payload) => {
    const newBasketItems = removeOneItem(state.basketItems, payload);
    const basketCount =
      newBasketItems.length > 0
        ? updateBasketInfo(newBasketItems, "quantity")
        : 0;
    const basketTotal =
      newBasketItems.length > 0
        ? updateBasketInfo(newBasketItems, "basketPrice")
        : 0;

    return {
      ...state,
      basketItems: newBasketItems,
      basketCount,
      basketTotal
    };
  },

  [basketActions.TOGGLE_BASKET_DRAWER]: (state, payload) => {
    return {
      ...state,
      basketOpen: payload
    };
  }
};

const basketReducer = (state = defaultBasketState, action) => {
  return handlers.hasOwnProperty(action.type)
    ? handlers[action.type](state, action.payload)
    : state;
};

export default basketReducer;
