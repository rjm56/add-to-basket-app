import { defaultBasketState } from "../state/basket-state";
import {
  mockProduct,
  mockAddedProductOne
} from "../../test-utils/test-product-data";
import basketReducer from "./basket-reducer";
import { basketActions } from "../actions";

describe("basket-reducer", () => {
  it("ADD_TO_BASKET", () => {
    const payload = mockProduct;

    const expectedState = {
      basketItems: [mockAddedProductOne],
      basketCount: 1,
      basketTotal: 9.25,
      basketOpen: false
    };

    let state = basketReducer(defaultBasketState, {
      type: basketActions.ADD_TO_BASKET,
      payload
    });

    expect(state).toEqual(expectedState);
  });

  it("REMOVE_ONE_FROM_BASKET", () => {
    const prevState = {
      basketItems: [mockAddedProductOne],
      basketCount: 1,
      basketTotal: 9.25,
      basketOpen: false
    };

    const payload = mockAddedProductOne;

    const expectedState = defaultBasketState;

    let state = basketReducer(prevState, {
      type: basketActions.REMOVE_ONE_FROM_BASKET,
      payload
    });

    expect(state).toEqual(expectedState);
  });

  it("TOGGLE_BASKET_DRAWER", () => {
    const payload = true;

    let state = basketReducer(defaultBasketState, {
      type: basketActions.TOGGLE_BASKET_DRAWER,
      payload
    });

    expect(state.basketOpen).toEqual(true);
  });
});
