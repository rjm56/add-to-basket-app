import * as React from "react";
import { ProductsContainer } from "../components/product-cards/container";
import { BasketDrawerContainer } from "../components/drawer";
import styled from "styled-components";
import { BasketIconContainer } from "../components/basket-icon/container";

const Wrapper = styled.div`
  padding-top: 60px;
`;

export const ProductPage = props => {
  return (
    <Wrapper>
      <ProductsContainer />
      <BasketIconContainer />
      <BasketDrawerContainer />
    </Wrapper>
  );
};
