import * as React from "react";
import Button from "@material-ui/core/Button";

export const PrimaryButton = props => {
  return (
    <Button
      onClick={props.handleClick}
      variant="flat"
      color="primary"
      fullWidth
    >
      {props.label}
    </Button>
  );
};
