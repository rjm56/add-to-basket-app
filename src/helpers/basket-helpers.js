export const updateBasketInfo = (basketItems, attribute) => {
  return basketItems
    .map(item => item[attribute])
    .reduce((prevQuant, nextQuant) => prevQuant + nextQuant);
};

export const addItem = (basketItems, payload) => {
  if (basketItems.length === 0) {
    return [{ ...payload, basketPrice: payload.productPrice, quantity: 1 }];
  } else {
    if (
      basketItems.findIndex(
        item => item.productCode === payload.productCode
      ) !== -1
    ) {
      return basketItems.map(item => {
        if (item.productCode === payload.productCode) {
          return {
            ...item,
            basketPrice: item.basketPrice + payload.productPrice,
            quantity: item.quantity + 1
          };
        } else {
          return item;
        }
      });
    } else {
      return [
        ...basketItems,
        { ...payload, basketPrice: payload.productPrice, quantity: 1 }
      ];
    }
  }
};

export const removeAllItems = (basketItems, payload) => {
  return basketItems.filter(item => item.productCode !== payload.productCode);
};

export const removeOneItem = (basketItems, payload) => {
  const foundItem = basketItems.find(
    item => item.productCode === payload.productCode
  );

  if (foundItem.quantity === 1) {
    return removeAllItems(basketItems, payload);
  } else {
    return basketItems.map(item => {
      if (item.productCode === payload.productCode) {
        return {
          ...item,
          basketPrice: item.basketPrice - payload.productPrice,
          quantity: item.quantity - 1
        };
      } else {
        return item;
      }
    });
  }
};
