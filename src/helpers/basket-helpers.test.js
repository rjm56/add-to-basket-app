import {
  updateBasketInfo,
  addItem,
  removeAllItems,
  removeOneItem
} from "./basket-helpers.js";
import {
  mockProduct,
  mockAddedProductOne,
  mockAddedProductTwo,
  mockAddedProducts
} from "../test-utils/test-product-data";

describe("updateBasketInfo", () => {
  it("should sum together all products' basketPrice if attribute is basketPrice", () => {
    const attribute = "basketPrice";
    const stateBasketItems = mockAddedProducts;
    const expectedPrice = 9.25 + 45;

    expect(updateBasketInfo(stateBasketItems, attribute)).toEqual(
      expectedPrice
    );
  });

  it("should sum together all products' quantities if attribute is quantity", () => {
    const attribute = "quantity";
    const stateBasketItems = mockAddedProducts;
    const expectedCount = 2;

    expect(updateBasketInfo(stateBasketItems, attribute)).toEqual(
      expectedCount
    );
  });
});

describe(
  "addItem()",
  () =>
    it("should return array with the payload product, payload product price and quantity: 1 if basket is empty", () => {
      const payload = mockProduct;
      const stateBasketItems = [];
      const newBasketItems = [mockAddedProductOne];

      expect(addItem(stateBasketItems, payload)).toEqual(newBasketItems);
    }),

  it("should increase quantity by one and price by payload.productPrice if basket already contains the same product", () => {
    const payload = mockProduct;
    const stateBasketItems = [mockAddedProductOne];
    const newBasketItems = [
      {
        productCode: "001",
        productName: "Travel Card Holder",
        productPrice: 9.25,
        basketPrice: 18.5,
        quantity: 2
      }
    ];

    expect(addItem(stateBasketItems, payload)).toEqual(newBasketItems);
  }),

  it("should increase quantity of matching product by one and price by payload.productPrice if basket already contains matching product plus other products", () => {
    const payload = mockProduct;
    const stateBasketItems = mockAddedProducts;
    const newBasketItems = [
      {
        productCode: "001",
        productName: "Travel Card Holder",
        productPrice: 9.25,
        basketPrice: 18.5,
        quantity: 2
      },
      {
        productCode: "002",
        productName: "Personalised Cufflinks",
        productPrice: 45,
        basketPrice: 45,
        quantity: 1
      }
    ];

    expect(addItem(stateBasketItems, payload)).toEqual(newBasketItems);
  }),

  it("should add product to the basket if basket does not already contain it", () => {
    const payload = mockProduct;
    const stateBasketItems = [mockAddedProductTwo];
    const newBasketItems = [
      {
        productCode: "002",
        productName: "Personalised Cufflinks",
        productPrice: 45,
        basketPrice: 45,
        quantity: 1
      },
      {
        productCode: "001",
        productName: "Travel Card Holder",
        productPrice: 9.25,
        basketPrice: 9.25,
        quantity: 1
      }
    ];

    expect(addItem(stateBasketItems, payload)).toEqual(newBasketItems);
  })
);

describe("removeAllItems()", () => {
  it("should remove all basket items with the same product code as the payload product", () => {
    const payload = mockAddedProductOne;
    const stateBasketItems = [
      {
        productCode: "001",
        productName: "Travel Card Holder",
        productPrice: 9.25,
        basketPrice: 9.25,
        quantity: 4
      },
      {
        productCode: "002",
        productName: "Personalised Cufflinks",
        productPrice: 45,
        basketPrice: 45,
        quantity: 1
      }
    ];
    const newBasketItems = [
      {
        productCode: "002",
        productName: "Personalised Cufflinks",
        productPrice: 45,
        basketPrice: 45,
        quantity: 1
      }
    ];

    expect(removeAllItems(stateBasketItems, payload)).toEqual(newBasketItems);
  });
});

describe("removeOneItem()", () => {
  it("should remove product completely from basket if product is found in the basket with a quantity of 1", () => {
    const payload = mockAddedProductOne;
    const stateBasketItems = mockAddedProducts;
    const newBasketItems = [mockAddedProductTwo];

    expect(removeOneItem(stateBasketItems, payload)).toEqual(newBasketItems);
  });
  it("should decrement quantity by one and decrease price by payload.productPrice if product is already in the basket", () => {
    const payload = mockAddedProductOne;
    const stateBasketItems = [
      {
        productCode: "001",
        productName: "Travel Card Holder",
        productPrice: 9.25,
        basketPrice: 18.5,
        quantity: 2
      },
      {
        productCode: "002",
        productName: "Personalised Cufflinks",
        productPrice: 45,
        basketPrice: 45,
        quantity: 1
      }
    ];
    const newBasketItems = mockAddedProducts;

    expect(removeOneItem(stateBasketItems, payload)).toEqual(newBasketItems);
  });
});
