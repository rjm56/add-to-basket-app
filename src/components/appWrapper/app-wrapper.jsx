import * as React from "react";
import { ProductPage } from "../../pages/product-page";

export const AppWrapper = () => {
  return (
    <div>
      <ProductPage />
    </div>
  );
};
