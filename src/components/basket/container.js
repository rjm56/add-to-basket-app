import { connect } from "react-redux";
import { removeOneFromBasket } from "../../store/actions";
import BasketContent from "./basket-content";

const mapStateToProps = state => {
  return {
    basketItems: state.basketState.basketItems,
    basketCount: state.basketState.basketCount,
    basketTotal: state.basketState.basketTotal
  };
};

const mapDispatchToProps = dispatch => {
  return {
    removeFromBasket(product) {
      dispatch(removeOneFromBasket(product));
    }
  };
};

export const BasketContentContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BasketContent);
