import * as React from "react";
import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const Wrapper = styled.div`
  padding: 32px;
`;

const TotalWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 24px;
`;

const NoItemsWrapper = styled.div`
  padding: 24px;
`;

const toCurrency = price => {
  return `£${price.toFixed(2)}`;
};

const tableStyles = theme => ({
  tableCell: {
    [theme.breakpoints.down("sm")]: {
      padding: 4
    }
  }
});

export const BasketContent = props => {
  const { classes } = props;

  const handleDelete = product => {
    props.removeFromBasket(product);
  };

  const renderTable = () => {
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableCell} />
            <TableCell className={classes.tableCell} />
            <TableCell className={classes.tableCell} numeric>
              Quantity
            </TableCell>
            <TableCell className={classes.tableCell} numeric>
              Total Price
            </TableCell>
            <TableCell className={classes.tableCell} numeric>
              Delete
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.basketItems.map((item, index) => {
            return (
              <TableRow key={index}>
                <TableCell
                  className={classes.tableCell}
                  component="th"
                  scope="row"
                >
                  <Avatar src={item.productImg} />
                </TableCell>
                <TableCell className={classes.tableCell}>
                  {item.productName}
                </TableCell>
                <TableCell className={classes.tableCell} numeric>
                  {item.quantity}
                </TableCell>
                <TableCell className={classes.tableCell} numeric>
                  {toCurrency(item.basketPrice)}
                </TableCell>
                <TableCell className={classes.tableCell} numeric>
                  <IconButton onClick={() => handleDelete(item)}>
                    <Icon>close</Icon>
                  </IconButton>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    );
  };

  const renderNoItems = () => {
    return (
      <NoItemsWrapper>
        <Typography variant="subheading">
          You have no items in your basket
        </Typography>
      </NoItemsWrapper>
    );
  };

  return (
    <Wrapper>
      <Typography variant="title" gutterBottom>
        {`Your Basket (${props.basketCount} ${
          props.basketCount === 1 ? "item" : "items"
        })`}
      </Typography>
      {props.basketCount > 0 ? renderTable() : renderNoItems()}
      <TotalWrapper>
        <Typography variant="headline">Total:</Typography>
        <Typography variant="headline">
          {toCurrency(props.basketTotal)}
        </Typography>
      </TotalWrapper>
    </Wrapper>
  );
};

export default withStyles(tableStyles)(BasketContent);
