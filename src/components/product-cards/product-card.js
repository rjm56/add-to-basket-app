import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import { PrimaryButton } from "../../atoms/button";

const cardStyles = {
  card: {
    width: 300,
    margin: 8,
    boxShadow: "0 0 4px 0 rgba(0,0,0,0.15)"
  },
  media: {
    height: 250
  }
};

const ProductCard = props => {
  const addToBasketClick = product => {
    props.addToBasket(product);
  };

  const { productName, productPrice, productImg } = props.product;
  const { classes } = props;
  return (
    <Card className={classes.card}>
      <CardMedia
        image={productImg}
        title={productName}
        className={classes.media}
      />
      <CardContent>
        <Typography component="h2" variant="headline">
          {productName}
        </Typography>
        <Typography component="p">{`£${productPrice.toFixed(2)}`}</Typography>
      </CardContent>
      <CardActions>
        <PrimaryButton
          label="Add to basket"
          handleClick={() => addToBasketClick(props.product)}
        />
      </CardActions>
    </Card>
  );
};

export default withStyles(cardStyles)(ProductCard);
