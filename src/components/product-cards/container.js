import { connect } from "react-redux";
import { addToBasket } from "../../store/actions";
import { ProductsWrapper } from "./products-wrapper";

const mapStateToProps = state => {
  return {
    products: state.productState.products
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addToBasket(product) {
      dispatch(addToBasket(product));
    }
  };
};

export const ProductsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsWrapper);
