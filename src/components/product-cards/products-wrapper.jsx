import * as React from "react";
import ProductCard from "./product-card";
import styled from "styled-components";
import { media } from "../../styles/breakpoints";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-left: 8px;
  margin-right: 8px;

  ${media.tablet`
    flex-direction:row;
    justify-content:center;
  `};
`;

export const ProductsWrapper = props => {
  return (
    <Wrapper>
      {props.products.map(product => {
        return (
          <ProductCard
            key={product.productCode}
            product={product}
            addToBasket={props.addToBasket}
          />
        );
      })}
    </Wrapper>
  );
};
