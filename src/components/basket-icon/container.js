import { connect } from "react-redux";
import { toggleBasketDrawer } from "../../store/actions";
import { BasketIcon } from "./basket-icon";

const mapStateToProps = state => {
  return {
    basketCount: state.basketState.basketCount
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleBasketDrawer(open) {
      dispatch(toggleBasketDrawer(open));
    }
  };
};

export const BasketIconContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BasketIcon);
