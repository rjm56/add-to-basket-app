import * as React from "react";
import styled from "styled-components";
import Icon from "@material-ui/core/Icon";

const IconWrapper = styled.div`
  position: absolute;
  top: 24px;
  right: 24px;
  height: 40px;
  width: 40px;
  &:hover {
    cursor: pointer;
    color: #4d4d4d;
  }
`;

const Badge = styled.div`
  background-color: #ff6666;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 11px;
  top: -9px;
  right: 9px;
  width: 18px;
  height: 18px;
  border-radius: 50%;
  color: #ffffff;
  transition: 0.3s ease-in-out;
  transform: ${props => (props.count > 0 ? "scale(1)" : "scale(0)")};
`;

export const BasketIcon = props => {
  return (
    <IconWrapper key={0} onClick={() => props.toggleBasketDrawer(true)}>
      <Icon fontSize={32}>shopping_cart</Icon>
      <Badge count={props.basketCount}>{props.basketCount}</Badge>
    </IconWrapper>
  );
};
