import * as React from "react";
import Drawer from "@material-ui/core/Drawer";
import { BasketContentContainer } from "../basket/container";
import { withStyles } from "@material-ui/core/styles";

const basketDrawerStyles = {
  paper: {
    minWidth: "55%",
    maxWidth: "90%"
  }
};

const BasketDrawer = props => {
  const { classes } = props;

  const handleCloseClick = () => {
    return props.toggleBasketDrawer(false);
  };

  return [
    <Drawer
      key={1}
      anchor="right"
      open={props.basketOpen}
      onClose={handleCloseClick}
      classes={{
        paper: classes.paper
      }}
    >
      <div tabIndex={0}>
        <BasketContentContainer />
      </div>
    </Drawer>
  ];
};

export default withStyles(basketDrawerStyles)(BasketDrawer);
