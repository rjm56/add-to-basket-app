import { connect } from "react-redux";
import { toggleBasketDrawer } from "../../store/actions";
import BasketDrawer from "./basket-drawer";

const mapStateToProps = state => {
  return {
    basketOpen: state.basketState.basketOpen
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleBasketDrawer(open) {
      dispatch(toggleBasketDrawer(open));
    }
  };
};

export const BasketDrawerContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BasketDrawer);
